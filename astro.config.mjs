// astro.config.mjs
import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  // GitLab Pages requires exposed files to be located in a folder called "public".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: 'public',

  // The folder name Astro uses for static files (`public`) is already reserved
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: 'static',
  
  base: '/mindcurrent-public/explore-jamstack-cmss/explore-astro-cms-ecosystems/repk8-starter_just-the-basics/just-the-basics_to-glp',
  // In this case, our project is owned by a subgroup, 
  // so we use: base: '/<subgroup>/<project-name>'

  site: 'https://just-the-basics-to-glp-mindcurrent-public-explor-685a3aeecfdf85.gitlab.io'
  // Site Spec: Our final, deployed URL.  Used tho generate sitemap & canonical URLs.
  // spec: https://docs.astro.build/en/reference/configuration-reference/#site
  // For now, presumably our GLP-generated site URL. 
 
});
